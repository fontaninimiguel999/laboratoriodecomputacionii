package desafios;

public class Desafio7 {

	public static void main(String[] args) {
		
		//Funciones trigonométricas habituales
		double num = Math.toRadians(22);
        System.out.println("Seno de 22= "+Math.sin(num));
        double num2 = Math.toRadians(22);
        System.out.println("Coseno de 22= "+Math.cos(num2));
        double num3 = Math.toRadians(22);
        System.out.println("Tangente de 22= "+Math.tan(num3));
        double num4 = Math.toRadians(22);
        System.out.println("Arco Tangente de 22= "+Math.atan(num4));
        double num5 = Math.toRadians(22);
        System.out.println("Arco2 Tangente de 22= "+Math.atan2(num5,num4));
        
        //La función exponencial y su inversa
        System.out.println("-------------------------------------");
        double num6 = Math.exp(5);
        System.out.println("exponente de 5= "+num6);
        double num7 = Math.log(5);
        System.out.println("logaritmo de 5= "+num7);//logaritmo en base e
        
        //Las dos constantes PI y e
        System.out.println("-------------------------------------");
        double num8 = Math.PI;
        System.out.println("PI= "+num8);
        double num9 = Math.E;
        System.out.println("Potencia de 2= "+num9);
        
	}

}
