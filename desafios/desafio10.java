package desafios;

import java.util.Scanner;

public class desafio10 {

    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);
        int intentos=0, numero;
        System.out.println("\t\t**ADIVINANZA**\n +El juego consiste en adivinar un número\n entre 1 y 100 en la menor cantidad de intentos.+");

        int numeroAleatorio = (int)(Math.random() * 100 + 1);
        System.out.println("*Número aleatorio creado*");

        do {
            System.out.println("Inserte el número: ");
            numero = scan.nextInt();
            while (numero<=0||numero>=100){
                System.out.println("El numero esta fuera de los limites(1-100)\nIntente otra vez: ");
                numero = scan.nextInt();
                }
            if (numero>numeroAleatorio) {
                System.out.println("El numero aleatorio es menor.");
            }
            if (numero<numeroAleatorio){
                System.out.println("El numero aleatorio es mayor.");
            }
            if (numero==numeroAleatorio){
                System.out.println("¡Acertó!");
            }
            intentos++;
        } while (numero!=numeroAleatorio);
        System.out.println("Intentos: "+intentos);


    }
}
